module.exports = {
  apps: [
    {
      name: "Frontend Server",
      script: "./server.js",
      instances: "max",
      autorestart: true,
      watch: false,
      max_memory_restart: "1G",
      exec_interpreter: "node@10.16.0",
      time: true,
      env: {
        NODE_ENV: "production"
      },
      env_production: {
        NODE_ENV: "production"
      }
    }
  ],

  deploy: {
    production: {
      user: "deploy",
      host: "anyfin.ordinary.systems",
      ref: "origin/master",
      repo: "git@gitlab.com:jibiabraham/anyfin-frontend.git",
      path: "/home/deploy/frontend_server"
    }
  }
};
