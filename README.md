### Requirements

1. Requires NodeJS (tested on version `10.16.0`)
2. Requires Nginx

## Setup instructions

1. Change values in `config.js` to point to your domain of choice. This domain must match the domain you created for running the api server for this project.
2. Run `npm install`
3. Run `npm run dev`
4. Your server should be up and running on the port you specified
5. You may also access the server via `http://localdev.com/home` (assuming default configuration).
6. 2 routes are available
   1. `/home` - Dashboard page
   2. `/login` - Login page

Note: The server may take a few moments to finish compiling the frontend. Up until this process is complete, you may receive an Nginx 5xx error while trying to access pages.

#### Sample Nginx configuration file

```
server {
  listen       80;
  server_name  localdev.com;

  access_log   /var/log/nginx/localdev.com.access_log;
  error_log    /var/log/nginx/localdev.com.error_log;

  location / {
    proxy_pass http://localhost:3000;
    proxy_set_header   X-Forwarded-For $remote_addr;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    #proxy_set_header Host $host;
    proxy_cache_bypass $http_upgrade;
  }

  location /static/ {
    proxy_pass http://localhost:3000;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    #proxy_set_header Host $host;
    proxy_cache_bypass $http_upgrade;
  }

  location /sockjs-node/ {
    proxy_pass http://localhost:3000;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
  }

  location /api/ {
    proxy_pass http://localhost:4044/;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    proxy_set_header Host $host;
    proxy_cache_bypass $http_upgrade;
  }
}

```
