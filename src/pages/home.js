import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import Navbar from "../components/Navbar";
import Alert from "../components/Alert";
import CountrySearch from "../components/Search";
import CountryList from "../components/CountryList";
import {
  findMatchingCountriesByName,
  addCountry,
  removeCountry,
  fetchMyList,
  logout
} from "../utils/api";
import { useDebounce } from "react-debounce-hook";
import isAuthenticated from "../utils/isAuthenticated";

const handleLoadCountries = (
  currencyConversionMap,
  setCurrencyConversionMap,
  router,
  setAlertMessage
) => async name => {
  let countryOptions = [];
  try {
    const response = await findMatchingCountriesByName(name);
    if (response.ok) {
      const {
        data: {
          matches,
          currencyConversionMap: additionalCurrencyConversionMap
        }
      } = await response.json();
      countryOptions = matches.map(country => ({
        label: country.name,
        value: country.name,
        data: country
      }));

      // Update currency conversion map with any new values
      setCurrencyConversionMap({
        ...currencyConversionMap,
        values: {
          ...currencyConversionMap.values,
          ...additionalCurrencyConversionMap.values
        }
      });
    } else if (response.status === 403) {
      // Tokens have expired and cannot be refreshed by the server
      // Needs user to login again
      setAlertMessage(
        "Your login has expired. Redirecting you to the login page."
      );
      await logout();
      router.push("/login");
    } else if (response.status === 429) {
      const {
        extended: { resetDurationMs }
      } = await response.json();
      const resetDurationSeconds = Math.ceil(resetDurationMs / 1000);
      const alertMessage = `Too many requests. Try again when this message disappears in ${resetDurationSeconds} seconds`;
      setAlertMessage(alertMessage, resetDurationMs);
    } else if (response.status >= 500) {
      // Server is unavailable
      // User has no other option but to wait until server is back up again
      setAlertMessage(
        "We are currently fixing a technical issue with this website. Please try again in some time."
      );
    }
  } catch (ex) {
    // This is most likely a client side code error
    // Use an error logger to post this error to an error logging service
    // Let the user know that further work is not possible right now
    console.error(ex);
    setAlertMessage(
      "We are currently fixing a technical issue with this website. Please try again in some time."
    );
  }
  return countryOptions;
};

const handleSelectCountry = (
  countryList,
  setCountryList,
  router,
  setAlertMessage
) => async selection => {
  const country = selection.data;
  try {
    const response = await addCountry(country.name);
    if (response.ok) {
      const existsInList = countryList.some(
        existingCountry => existingCountry.name === country.name
      );
      if (!existsInList) {
        setCountryList([country, ...countryList]);
      }
    } else if (response.status === 403) {
      // Tokens have expired and cannot be refreshed by the server
      // Needs user to login again
      setAlertMessage(
        "Your login has expired. Redirecting you to the login page."
      );
      await logout();
      router.push("/login");
    } else if (response.status === 429) {
      const {
        extended: { resetDurationMs }
      } = await response.json();
      const resetDurationSeconds = Math.ceil(resetDurationMs / 1000);
      const alertMessage = `Too many requests. Try again when this message disappears in ${resetDurationSeconds} seconds`;
      setAlertMessage(alertMessage, resetDurationMs);
    } else if (response.status >= 500) {
      // Server is unavailable
      // User has no other option but to wait until server is back up again
      setAlertMessage(
        "We are currently fixing a technical issue with this website. Please try again in some time."
      );
    }
  } catch (ex) {
    // This is most likely a client side code error
    // Use an error logger to post this error to an error logging service
    // Let the user know that further work is not possible right now
    console.error(ex);
    setAlertMessage(
      "We are currently fixing a technical issue with this website. Please try again in some time."
    );
  }
};

const handleRemoveCountry = (countryList, setCountryList) => async country => {
  try {
    const response = await removeCountry(country.name);
    if (response.ok) {
      setCountryList(countryList.filter(item => item.name !== country.name));
    } else if (response.status === 403) {
      // Tokens have expired and cannot be refreshed by the server
      // Needs user to login again
      setAlertMessage(
        "Your login has expired. Redirecting you to the login page."
      );
      await logout();
      router.push("/login");
    } else if (response.status === 429) {
      const {
        extended: { resetDurationMs }
      } = await response.json();
      const resetDurationSeconds = Math.ceil(resetDurationMs / 1000);
      const alertMessage = `Too many requests. Try again when this message disappears in ${resetDurationSeconds} seconds`;
      setAlertMessage(alertMessage, resetDurationMs);
    } else if (response.status >= 500) {
      // Server is unavailable
      // User has no other option but to wait until server is back up again
      setAlertMessage(
        "We are currently fixing a technical issue with this website. Please try again in some time."
      );
    }
  } catch (ex) {
    // This is most likely a client side code error
    // Use an error logger to post this error to an error logging service
    // Let the user know that further work is not possible right now
    console.error(ex);
    setAlertMessage(
      "We are currently fixing a technical issue with this website. Please try again in some time."
    );
  }
};

const useSelectDebounce = loadOptions => {
  const [callback, setCallback] = useState();
  const [searchTerm, setSearchTerm] = useState();
  const handleSearch = async term =>
    callback && callback(await loadOptions(term));

  useDebounce(searchTerm, handleSearch);

  return { setCallback, setSearchTerm };
};

const Home = ({
  redirectToLoginPage,
  initialAlertMessage,
  hasValidToken,
  loggedInUserName,
  userCountryList = [],
  userCurrencyConversionMap = {
    baseCurrencyCode: "SEK",
    baseCurrencyAmount: 1,
    values: {}
  }
}) => {
  const router = useRouter();

  useEffect(() => {
    if (!hasValidToken || redirectToLoginPage) {
      logout();
      router.push("/login");
    }
  }, [hasValidToken, redirectToLoginPage]);

  const [baseCurrencyAmount, setBaseCurrencyAmount] = useState(1);
  const [countryList, setCountryList] = useState(userCountryList);
  const [currencyConversionMap, setCurrencyConversionMap] = useState(
    userCurrencyConversionMap
  );
  const [alertMessage, setAlertMessage] = useState(initialAlertMessage);
  const setExpiringAlert = (message, timer = 5000) => {
    setAlertMessage(message);
    setTimeout(() => setAlertMessage(null), timer);
  };
  const { setSearchTerm, setCallback } = useSelectDebounce(
    handleLoadCountries(
      currencyConversionMap,
      setCurrencyConversionMap,
      router,
      setExpiringAlert
    )
  );

  const handleLoadOptions = (term, callback) => {
    setCallback(() => callback);
    setSearchTerm(term);
  };

  const handleBaseCurrencyAmountChange = ({ target: { value } }) =>
    setBaseCurrencyAmount(parseInt(value, 10) || 0);

  return (
    <div className="main min-h-screen flex flex-col">
      <Navbar
        hasValidToken={hasValidToken}
        loggedInUserName={loggedInUserName}
      />
      <div className="container flex flex-col items-center mx-auto">
        <div className="w-full mt-6 flex justify-between">
          <div className="w-3/6 flex flex-col">
            <div className="">
              <CountrySearch
                onLoadOptions={handleLoadOptions}
                onSelectEntry={handleSelectCountry(
                  countryList,
                  setCountryList,
                  router,
                  setExpiringAlert
                )}
              />
            </div>
          </div>
          <div className="">
            <div className="h-10 px-4 flex items-center border border-gray-400 rounded">
              <input
                className="w-full px-2 text-lg"
                type="text"
                placeholder="Enter an amount here"
                value={baseCurrencyAmount}
                onChange={handleBaseCurrencyAmountChange}
              />
            </div>
          </div>
        </div>
        <div className="w-full mt-4">
          <CountryList
            countryList={countryList}
            currencyConversionMap={currencyConversionMap}
            amountToConvert={baseCurrencyAmount}
            onRemoveCountry={handleRemoveCountry(
              countryList,
              setCountryList,
              router,
              setExpiringAlert
            )}
          />
        </div>
      </div>
      <Alert text={alertMessage} />
    </div>
  );
};

Home.getInitialProps = async ({ req, res }) => {
  const baseCurrencyCode = "SEK";
  let userCountryList;
  let userCurrencyConversionMap;

  const { hasValidToken, loggedInUserName } = isAuthenticated(
    req && req.headers.cookie
  );

  let redirectToLoginPage = false;
  if (!hasValidToken) {
    if (res) {
      res.redirect("/login");
    }
    return {
      redirectToLoginPage: true,
      hasValidToken,
      loggedInUserName,
      userCountryList,
      userCurrencyConversionMap
    };
  }

  let initialAlertMessage = null;
  try {
    const response = await fetchMyList(
      baseCurrencyCode,
      req && req.headers.cookie,
      res
    );

    if (response.ok) {
      const { data } = await response.json();
      const user = data.user || {
        countries: [],
        currencyConversionMap: {
          baseCurrencyCode,
          baseCurrencyAmount: 1,
          values: {}
        }
      };
      userCountryList = user.countries;
      userCurrencyConversionMap = user.currencyConversionMap;
    } else if (response.status === 403) {
      // Tokens have expired and cannot be refreshed by the server
      // Needs user to login again
      redirectToLoginPage = true;
      initialAlertMessage =
        "Your login has expired. Redirecting you to the login page.";
    } else if (response.status === 429) {
      const {
        extended: { resetDurationMs }
      } = await response.json();
      const resetDurationSeconds = Math.ceil(resetDurationMs / 1000);
      initialAlertMessage = `Too many requests. Try again when this message disappears in ${resetDurationSeconds} seconds`;
    } else if (response.status >= 500) {
      // Server is unavailable
      // User has no other option but to wait until server is back up again
      initialAlertMessage =
        "We are currently fixing a technical issue with this website. Please try again in some time.";
    }
  } catch (ex) {
    // This is most likely a client side code error
    // Use an error logger to post this error to an error logging service
    // Let the user know that further work is not possible right now
    console.error(ex);
    initialAlertMessage =
      "We are currently fixing a technical issue with this website. Please try again in some time.";
  }

  return {
    redirectToLoginPage,
    initialAlertMessage,
    hasValidToken,
    loggedInUserName,
    userCountryList,
    userCurrencyConversionMap
  };
};

export default Home;
