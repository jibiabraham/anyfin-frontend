import { useState } from "react";
import Alert from "../components/Alert";
import { WhenLoggedIn, WhenLoggedOut } from "../components/Login";
import { login, logout } from "../utils/api";
import isAuthenticated from "../utils/isAuthenticated";

const handleLogin = (
  setUserName,
  setIsAuthenticated,
  setAlertMessage
) => async userName => {
  try {
    const response = await login(userName);
    if (response.ok) {
      const {
        data: { accessTokenUserName }
      } = await response.json();
      setUserName(accessTokenUserName);
      setIsAuthenticated(true);
    } else if (response.status === 422) {
      // There is an user correctable error with a request parameter
      // Alert the user to fix error and retry
      const { name, message, extended = {} } = await response.json();
      const serverMessages = Object.values(extended);
      const alertMessage =
        serverMessages.length > 0 ? serverMessages.join("\n") : message;
      setAlertMessage(alertMessage);
    } else if (response.status >= 500) {
      // Server is unavailable
      // User has no other option but to wait until server is back up again
      setAlertMessage(
        "We are currently fixing a techinical issue with this website. Please try again in some time."
      );
    }
  } catch (ex) {
    // This is most likely a client side code error
    // Use an error logger to post this error to an error logging service
    // Let the user know that further work is not possible right now
    console.error(ex);
    setAlertMessage(
      "We are currently fixing a techinical issue with this website. Please try again in some time."
    );
  }
};

const handleLogout = setIsAuthenticated => async () => {
  await logout();
  setIsAuthenticated(false);
};

const Page = ({ hasValidToken, loggedInUserName }) => {
  const [userName, setUserName] = useState(loggedInUserName);
  const [isAuthenticated, setIsAuthenticated] = useState(hasValidToken);
  const [alertMessage, setAlertMessage] = useState(null);
  const setExpiringAlert = (message, timer = 2500) => {
    setAlertMessage(message);
    setTimeout(() => setAlertMessage(null), timer);
  };
  return (
    <div className="main min-h-screen flex relative">
      <div className="p-4 flex flex-col self-center border border-gray-200 rounded-md mx-auto my-auto">
        {isAuthenticated && (
          <WhenLoggedIn
            name={userName}
            onLogout={handleLogout(setIsAuthenticated)}
          />
        )}
        {!isAuthenticated && (
          <WhenLoggedOut
            onLogin={handleLogin(
              setUserName,
              setIsAuthenticated,
              setExpiringAlert
            )}
          />
        )}
      </div>
      <Alert text={alertMessage} />
    </div>
  );
};

Page.getInitialProps = async ({ req }) => {
  const { hasValidToken, loggedInUserName } = isAuthenticated(
    req && req.headers.cookie
  );
  return {
    hasValidToken,
    loggedInUserName
  };
};

export default Page;
