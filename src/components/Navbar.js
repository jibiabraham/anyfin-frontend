import Link from "next/link";

const Navbar = ({ hasValidToken, loggedInUserName }) => (
  <div className="w-full bg-gray-800">
    <div className="container h-12 flex items-center justify-between mx-auto">
      <div className="-ml-4 px-4 text-gray-200">
        <Link href="/home">
          <a href="#">Dashboard</a>
        </Link>
      </div>
      <div className="-mr-4 px-4 text-gray-200">
        {hasValidToken && loggedInUserName && (
          <Link href="/login">
            <a href="#">Logged in as {loggedInUserName}</a>
          </Link>
        )}
      </div>
    </div>
  </div>
);

export default Navbar;
