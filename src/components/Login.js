import { useState } from "react";
import Link from "next/link";

export const WhenLoggedIn = ({ name, onLogout }) => (
  <div className="h-56 w-64 flex flex-col justify-between">
    <div>
      <h1 className="text-5xl">Hello, {name}</h1>
    </div>
    <div className="flex flex-col items-center">
      <Link href="/home">
        <button className="w-full py-2 px-4 self-end bg-gray-200 hover:bg-gray-100 text-gray-700 border border-gray-400 rounded shadow focus:outline-none">
          Go to dashboard
        </button>
      </Link>
      <span className="mt-4 text-sm">
        Not {name}?{" "}
        <a className="text-blue-600" href="#" onClick={onLogout}>
          Login as a different user
        </a>
      </span>
    </div>
  </div>
);

export const WhenLoggedOut = ({ onLogin }) => {
  const [userName, setUserName] = useState("");
  const onUserNameChange = ({ target: { value } }) => setUserName(value);

  return (
    <form
      className="h-56 w-64 flex flex-col justify-between"
      onSubmit={e => {
        onLogin(userName);
        e.preventDefault();
      }}
    >
      <div>
        <h1 className="text-5xl">Hello,</h1>
        <input
          className="w-full px-4 py-2 border border-gray-200 rounded-md text-xl"
          type="text"
          name="userName"
          value={userName}
          onChange={onUserNameChange}
          placeholder="Your name here"
          required
        />
      </div>
      <button className="w-full py-2 px-4 self-end bg-gray-200 hover:bg-gray-100 text-gray-700 border border-gray-400 rounded shadow focus:outline-none">
        Login {userName ? `as ${userName}` : ""}
      </button>
    </form>
  );
};
