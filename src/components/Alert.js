const Alert = ({ text }) => {
  return text && text.length > 0 ? (
    <div className="absolute w-full mt-auto mb-4 bottom-0">
      <div className="h-10 container flex items-center bg-red-300 mx-auto justify-center">
        <h3 className="text-gray-800">{text}</h3>
      </div>
    </div>
  ) : (
    <></>
  );
};

export default Alert;
