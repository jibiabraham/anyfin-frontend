import AsyncSelect from "react-select/async";

const selectStyles = {
  container: provided => ({
    ...provided,
    height: "2.5rem"
  }),
  control: provided => ({
    ...provided,
    height: "2.5rem"
  })
};

const Search = ({ onLoadOptions, onSelectEntry }) => {
  return (
    <AsyncSelect
      styles={selectStyles}
      cacheOptions
      loadOptions={onLoadOptions}
      onChange={onSelectEntry}
    />
  );
};

export default Search;
