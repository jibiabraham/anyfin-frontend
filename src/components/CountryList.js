const Country = ({
  name,
  flag,
  population,
  numberFormatter,
  currencies,
  currencyConversionMap,
  amountToConvert,
  onRemoveCountry
}) => (
  <div className="mt-8 flex border border-b-8 justify-between">
    <div className="flex">
      <div className="p-4 bg-gray-300">
        <div
          className="h-32 w-32 p-4"
          style={{
            background: `center / contain url(${flag}) no-repeat`
          }}
        ></div>
      </div>
      <div className="ml-2 flex flex-col justify-evenly">
        <div>
          <h1 className="text-2xl">{name}</h1>
          <h5 className="text-base">
            Population: {numberFormatter.format(population)}
          </h5>
        </div>
        <div>
          {currencies.map(({ name, code }, idx) => (
            <div key={code} className="flex">
              <div className="text-lg">{idx + 1}.</div>
              <div className="ml-2">
                <h2 className="text-lg">
                  {name} ({code})
                </h2>
                {currencyConversionMap.values[code] && (
                  <>
                    {amountToConvert <= 1 && (
                      <h3>
                        {currencyConversionMap.baseCurrencyAmount}{" "}
                        {currencyConversionMap.baseCurrencyCode} ={" "}
                        {numberFormatter.format(
                          currencyConversionMap.values[code]
                        )}{" "}
                        {code}
                      </h3>
                    )}
                    {amountToConvert > 1 && (
                      <h3>
                        {numberFormatter.format(amountToConvert)}{" "}
                        {currencyConversionMap.baseCurrencyCode} ={" "}
                        {numberFormatter.format(
                          amountToConvert * currencyConversionMap.values[code]
                        )}{" "}
                        {code}
                      </h3>
                    )}
                  </>
                )}
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
    <div className="relative text-lg">
      <button
        className="px-2 py-1 top-0 right-0 text-sm text-red-400 hover:text-red-600"
        onClick={onRemoveCountry}
      >
        Remove
      </button>
    </div>
  </div>
);

const CountryList = ({
  countryList,
  currencyConversionMap,
  amountToConvert,
  onRemoveCountry
}) => {
  const numberFormatter = new Intl.NumberFormat();
  return countryList.map(country => (
    <Country
      key={country.name}
      {...country}
      numberFormatter={numberFormatter}
      currencyConversionMap={currencyConversionMap}
      amountToConvert={amountToConvert}
      onRemoveCountry={e => onRemoveCountry(country)}
    />
  ));
};

export default CountryList;
