import Cookies from "universal-cookie";

function isAuthenticated(cookieString) {
  const cookies = cookieString ? new Cookies(cookieString) : new Cookies();
  const accessTokenUserName = cookies.get("accessTokenUserName");
  const accessTokenExpiresAt = cookies.get("accessTokenExpiresAt");
  const refreshTokenExpiresAt = cookies.get("refreshTokenExpiresAt");

  const now = Date.now();
  const hasValidAccessToken =
    accessTokenExpiresAt && accessTokenExpiresAt > now;
  const hasValidRefreshToken = refreshTokenExpiresAt > now;

  if (!hasValidAccessToken && !hasValidRefreshToken) {
    return {
      hasValidToken: false,
      loggedInUserName: null
    };
  }

  return {
    hasValidToken: true,
    loggedInUserName: accessTokenUserName
  };
}

export default isAuthenticated;
