function ApiError(message, code, extended) {
  Error.call(this);
  this.name = "ApiError";
  this.message = message;
  this.code = code;
  this.extended = extended;
}

function createApiError(message, code, extended = {}) {
  return new ApiError(message, code, extended);
}

module.exports = createApiError;
