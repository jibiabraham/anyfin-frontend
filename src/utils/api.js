import fetch from "isomorphic-unfetch";
import { API } from "../../config";

export async function login(userName) {
  const url = `${API}/auth/login`;
  const method = "POST";
  const body = JSON.stringify({ userName });
  const headers = { "Content-Type": "application/json" };
  return await fetch(url, { method, body, headers });
}

export async function logout(cookies) {
  const url = `${API}/auth/logout`;
  const method = "POST";
  const headers = { "Content-Type": "application/json" };
  if (cookies) {
    headers.Cookie = cookies;
  }
  await fetch(url, { method, headers, credentials: "include" });
}

export async function findMatchingCountriesByName(name) {
  const url = `${API}/v1/country/name/${name}`;
  const method = "GET";
  const headers = { "Content-Type": "application/json" };
  return await fetch(url, {
    method,
    headers
  });
}

export async function addCountry(countryName) {
  const url = `${API}/v1/user/add-country/${countryName}`;
  const method = "POST";
  const headers = { "Content-Type": "application/json" };
  return await fetch(url, {
    method,
    headers,
    credentials: "include"
  });
}

export async function removeCountry(countryName) {
  const url = `${API}/v1/user/remove-country/${countryName}`;
  const method = "POST";
  const headers = { "Content-Type": "application/json" };
  return await fetch(url, {
    method,
    headers,
    credentials: "include"
  });
}

export async function fetchMyList(
  baseCurrencyCode,
  cookies,
  serverSideResponse
) {
  const url = `${API}/v1/user/me/${baseCurrencyCode}`;
  const method = "GET";
  const headers = { "Content-Type": "application/json" };
  if (cookies) {
    headers.Cookie = cookies;
  }
  const response = await fetch(url, {
    method,
    headers,
    credentials: "include"
  });

  // Set any response cookies on the client
  const responseCookies = response.headers.get("set-cookie");
  if (responseCookies && serverSideResponse) {
    serverSideResponse.set("set-cookie", responseCookies);
  }

  return response;
}
